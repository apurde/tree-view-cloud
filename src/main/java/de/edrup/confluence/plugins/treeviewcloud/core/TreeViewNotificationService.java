package de.edrup.confluence.plugins.treeviewcloud.core;

import java.util.Locale;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.atlassian.connect.spring.AtlassianHost;

import de.edrup.confluence.plugins.treeviewcloud.util.MiniJSONDotAccess;
import de.edrup.confluence.plugins.treeviewcloud.util.TreeViewHelper;

@Service
public class TreeViewNotificationService {
	
	@Autowired
	private TreeViewHelper treeViewHelper;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private TreeViewSMTP treeViewSMTP;
	
	private static final MiniJSONDotAccess da = new MiniJSONDotAccess();
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewNotificationService.class);
	
	
	public void notityPageAdded(AtlassianHost host, String accountNameTriggering, String accountIdToNotify, String accountNameToNotify, JSONObject page, JSONObject watchPage, JSONObject settings) {
		nofityUsingTemplates(host, accountNameTriggering, accountIdToNotify, accountNameToNotify, page, watchPage,
			da.getString(settings, "addedTitle", messageSource.getMessage("tree-view-cloud.conf.default.added-title", null, Locale.getDefault())),
			da.getString(settings, "addedBody", messageSource.getMessage("tree-view-cloud.conf.default.added-body", null, Locale.getDefault())), settings);
	}
	
	
	public void notityPageMovedOut(AtlassianHost host, String accountNameTriggering, String accountIdToNotify, String accountNameToNotify, JSONObject page, JSONObject watchPage, JSONObject settings) {
		nofityUsingTemplates(host, accountNameTriggering, accountIdToNotify, accountNameToNotify, page, watchPage,
			da.getString(settings, "movedoutTitle", messageSource.getMessage("tree-view-cloud.conf.default.movedout-title", null, Locale.getDefault())),
			da.getString(settings, "movedoutBody", messageSource.getMessage("tree-view-cloud.conf.default.movedout-body", null, Locale.getDefault())), settings);
	}
	
	
	private void nofityUsingTemplates(AtlassianHost host, String accountNameTriggering, String accountIdToNotify, String accountNameToNotify, JSONObject page, JSONObject watchPage, String titleTemplate, String bodyTemplate, JSONObject settings) {
		String email = getEmail(host, accountIdToNotify);
		String title = enrichTemplate(host, titleTemplate, accountNameToNotify, page, watchPage);
		String body = enrichTemplate(host, bodyTemplate, accountNameToNotify, page, watchPage);
		log.debug("Notifying {}:{} with subject {} and body {}", accountIdToNotify, maskEmail(email), title, removeHTML(body));
		if(!email.isEmpty() && !title.isEmpty() && !body.isEmpty()) {
			if(da.getString(settings, "mailServer", "internal").equals("internal") || !da.getBoolean(settings, "hashValid", true)) {
				sendEmailSES(email, title, removeHTML(body), body, accountNameTriggering);
			}
			else {
				treeViewSMTP.sendEmail(email, title, removeHTML(body), body, accountNameTriggering, settings);
			}
		}
	}
	
	
	private String getEmail(AtlassianHost host, String accountId) {
		if("557058:f86bb137-bcc1-4fee-b58b-f12e0885b19c".equals(accountId)) {
			return "tu1@purde.de";
		}
		else {
			return da.getString(treeViewHelper.getJSONObject(host, accountId, String.format("%s/rest/api/user/email?accountId=%s", host.getBaseUrl(), accountId)), "email", "");
		}
	}
	
	
	private String enrichTemplate(AtlassianHost host, String template, String accountNameToNotify, JSONObject page, JSONObject watchPage) {
		return template.replace("$recipient", accountNameToNotify)
			.replace("$pageTitleWithLink", String.format("<a href=\"%s\">%s</a>", host.getBaseUrl() + da.getString(page, "_links.webui", "#"), HtmlUtils.htmlEscape(da.getString(page, "title", "unknown"))))
			.replace("$watchTitleWithLink", String.format("<a href=\"%s\">%s</a>", host.getBaseUrl() + da.getString(watchPage, "_links.webui", "#"), HtmlUtils.htmlEscape(da.getString(watchPage, "title", "unknown"))))
			.replace("$pageTitle", HtmlUtils.htmlEscape(da.getString(page, "title", "unknown")))
			.replace("$watchTitle", HtmlUtils.htmlEscape(da.getString(watchPage, "title", "unknown")))
			.replace("$body", da.getString(page, "body.export_view.value", "?"));
	}
	
	
	private String removeHTML(String body) {
		return HtmlUtils.htmlUnescape(body.replaceAll("(?s)<script.*?<\\/script>", "").replaceAll("(?s)<.*?>", " ").replaceAll("(?s) +", " ").trim());
	}

	
	private void sendEmailSES(String to, String subject, String bodyText, String bodyHTML, String fromName) {
		try {
			AmazonSimpleEmailService client =  AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.EU_CENTRAL_1).build();
			 SendEmailRequest request = new SendEmailRequest()
		          .withDestination(new Destination().withToAddresses(to))
		          .withMessage(new Message()
		              .withBody(new Body()
		                  .withHtml(new Content().withCharset("UTF-8").withData(bodyHTML))
		                  .withText(new Content().withCharset("UTF-8").withData(bodyText)))
		              .withSubject(new Content().withCharset("UTF-8").withData(subject)))
		          .withSource(fromName + " <noreply@purde.eu>");
			 client.sendEmail(request);
		}
		catch(Exception e) {
			log.warn("Could not send message to {}: {}", maskEmail(to), e.toString());
			log.trace("Trace:", e);
		}
	}
	
	
	private String maskEmail(String email) {
		return email.replaceAll("(^[^@]{3}|(?!^)\\G)[^@]", "$1*");
	}
}
