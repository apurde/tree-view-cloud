package de.edrup.confluence.plugins.treeviewcloud.util;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class TreeViewHelper {
	
	@Autowired
	private AtlassianHostRestClients atlassianHostRestClients;
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewHelper.class);
	
	private static final int GCM_TAG_LENGTH = 128; // in bits
	private static final int GCM_IV_LENGTH = 12;   // in bytes
	
	
	public JSONObject getJSONObject(AtlassianHost host, String useAccountId, String url) {
		try {
			return new JSONObject(authenticatedProvider(host, useAccountId).getForObject(url, String.class));
		}
		catch(Exception e) {
			log.error("Could not get {} using accountId {}: {}", url, useAccountId, e.toString());
			return new JSONObject();			
		}
	}
	
	
	public boolean delete(AtlassianHost host, String useAccountId, String url) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<>(null, headers);
			authenticatedProvider(host, useAccountId).exchange(url, HttpMethod.DELETE, entity, String.class);
			return true;
		}
		catch(Exception e) {
			log.warn("Could not delete {} using accountId {}: {}", url, useAccountId, e.toString());
			return false;
		}
	}
	
	
	public ResponseEntity<String> exchangeStringPayload(AtlassianHost host, String useAccountId, HttpMethod method, String url, String payload) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(payload, headers);
			return authenticatedProvider(host, useAccountId).exchange(url, method, entity, String.class);
		}
		catch(Exception e) {
			log.error("Could not exchange {} using accountId {}: {}", url, useAccountId, e.toString());
			return new ResponseEntity<String>(buildJSONErrorMessage(e.toString()), HttpStatus.BAD_REQUEST);
		}
	}
	
	
	public JSONObject getJSONObjectAsApp(String url) {
		try {
			return new JSONObject(atlassianHostRestClients.authenticatedAsAddon().getForObject(url, String.class));
		}
		catch(Exception e) {
			log.error("Could not get {} as app: {}", url, e.toString());
			return new JSONObject();			
		}
	}
	
	
	public boolean deleteAsApp(String url) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<>(null, headers);
			atlassianHostRestClients.authenticatedAsAddon().exchange(url, HttpMethod.DELETE, entity, String.class);
			return true;
		}
		catch(Exception e) {
			log.error("Could not delete {} as app: {}", url, e.toString());
			return false;			
		}
	}
	
	
	private RestTemplate authenticatedProvider(AtlassianHost host, String useAccountId) {
		if(useAccountId != null && !useAccountId.isEmpty()) {
			return atlassianHostRestClients.authenticatedAs(AtlassianHostUser.builder(host).withUserAccountId(useAccountId).build());
		}
		else {
			return atlassianHostRestClients.authenticatedAsAddon(host);
		}
	}
	
	
	public String buildJSONErrorMessage(String message) {
		return new JSONObject().put("error", message).toString();
	}
	
	
	public String encryptString(String text, String key) {
        try {
            // Generate a random 12-byte IV for GCM (recommended size)
            byte[] iv = new byte[GCM_IV_LENGTH];
            SecureRandom random = new SecureRandom();
            random.nextBytes(iv);

            // Create AES key from the provided key bytes.
            // (Note: In a real-world scenario, consider using a KDF such as PBKDF2.)
            Key aesKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            // Initialize cipher for AES/GCM/NoPadding
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH, iv);
            cipher.init(Cipher.ENCRYPT_MODE, aesKey, spec);

            // Encrypt the plaintext (we use UTF-8 encoding)
            byte[] cipherText = cipher.doFinal(text.getBytes("UTF-8"));

            // Combine IV and ciphertext (IV is needed for decryption)
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            outputStream.write(iv);
            outputStream.write(cipherText);
            byte[] finalData = outputStream.toByteArray();

            // Base64-encode the combined result and add a version marker.
            String encryptedData = Base64.getEncoder().encodeToString(finalData);
            return "v2:" + encryptedData;
        } catch (Exception e) {
            // In production, log the error appropriately.
            return "";
        }
    }


    public String decryptString(String encrypted, String key) {
        try {
            if (encrypted.startsWith("v2:")) {
                // New algorithm: Remove the "v2:" prefix.
                String encryptedPayload = encrypted.substring(3);
                byte[] decoded = Base64.getDecoder().decode(encryptedPayload);

                // Make sure there are enough bytes to extract the IV.
                if (decoded.length < GCM_IV_LENGTH) {
                    return "";
                }

                // Extract the IV and the ciphertext
                byte[] iv = Arrays.copyOfRange(decoded, 0, GCM_IV_LENGTH);
                byte[] cipherText = Arrays.copyOfRange(decoded, GCM_IV_LENGTH, decoded.length);

                Key aesKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
                Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
                GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH, iv);
                cipher.init(Cipher.DECRYPT_MODE, aesKey, spec);
                byte[] plainText = cipher.doFinal(cipherText);
                return new String(plainText, "UTF-8");
            } else {
                // Fallback to the legacy decryption for backwards compatibility.
                Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
                Cipher cipher = Cipher.getInstance("AES");
                cipher.init(Cipher.DECRYPT_MODE, aesKey);
                byte[] decoded = Base64.getDecoder().decode(encrypted);
                String decrypted = new String(cipher.doFinal(decoded));

                // Remove the appended salt (e.g. "{SALT:...}") from the legacy encryption.
                return decrypted.replaceFirst("\\{SALT:.*?\\}$", "");
            }
        } catch (Exception e) {
            // In production, log the error appropriately.
            return "";
        }
    }
    
    
	public boolean isAdmin(AtlassianHost host, String accountId) {
		try {
			JSONArray operations = getJSONObject(host, accountId, String.format("%s/rest/api/user/current?expand=operations", host.getBaseUrl())).getJSONArray("operations");
			for(int n = 0; n < operations.length(); n++) {
				if(operations.getJSONObject(n).getString("operation").equals("administer") && operations.getJSONObject(n).getString("targetType").equals("application")) {
					return true;
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		return false;
	}
}
