package de.edrup.confluence.plugins.treeviewcloud.controller;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ContextJwt;
import com.atlassian.connect.spring.IgnoreJwt;

import de.edrup.confluence.plugins.treeviewcloud.core.TreeViewCore;
import de.edrup.confluence.plugins.treeviewcloud.core.TreeViewSMTP;
import de.edrup.confluence.plugins.treeviewcloud.util.SafeSettings;

@Controller
public class TreeViewController {
	
	@Autowired
	private TreeViewCore treeViewCore;
	
	@Autowired
	private TreeViewSMTP treeViewSMTP;
	
	@Autowired
	private SafeSettings safeSettings;
	
	@Value("${SECRET}")
    private String statusSecret;
		
	
	@RequestMapping(value = {"/configure"}, method = RequestMethod.GET)
	public ModelAndView configure() {
	    ModelAndView model = new ModelAndView("configure");	    
	    return model;
	}
	
	
	@RequestMapping(value = {"/dialog"}, method = RequestMethod.GET)
	public ModelAndView dialog(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="contentId") String contentId,
		@RequestParam(value="spaceKey") String spaceKey,
		@RequestParam(value="lic", defaultValue="") String lic) {
		
		ModelAndView model = new ModelAndView("tree-watch-dialog");
		model.addObject("pageId", contentId);
		model.addObject("spaceKey", spaceKey);
		model.addObject("accountId", hostUser.getUserAccountId().get());
		model.addObject("licensed", lic.equals("active") || hostUser.getHost().getBaseUrl().contains("edrup-dev"));
		
		return model;
	}
	
	
	@RequestMapping(value = {"/resolver"}, method = RequestMethod.GET)
	public ModelAndView resolver(@RequestParam(value="contentID", defaultValue="0") String contentId) {
		
	    ModelAndView model = new ModelAndView("resolver");
	    model.addObject("contentId", contentId);
	    
	    return model;
	}
	
	
	@RequestMapping(value = {"/overview"}, method = RequestMethod.GET)
	public ModelAndView topicsView(@AuthenticationPrincipal AtlassianHostUser hostUser) {
	
		String baseUrl = hostUser.getHost().getBaseUrl();
		
	    ModelAndView model = new ModelAndView("tree-watch-overview");
	    model.addObject("baseUrl", baseUrl);
	    model.addObject("accountId", hostUser.getUserAccountId().orElse("unknown"));
	    return model;
	}
	
	
	@RequestMapping(value = "/migration", method = RequestMethod.GET)
	public ModelAndView migration() {				
	    ModelAndView model = new ModelAndView("migration");
	    return model;
	}
	
	
	@RequestMapping(value = {"/watch-tree"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> treeWatchPage(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="pageId") String pageId, @RequestParam(value="set") Boolean set) {
		treeViewCore.setWatchFlags(hostUser.getHost(), hostUser.getUserAccountId().get(), pageId, set);
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/webhook/addedMoved"}, method = RequestMethod.POST)
	public ResponseEntity<String> addedMoved(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		treeViewCore.handlePageAddedOrMovedWebhook(hostUser.getHost(),  new JSONObject(payload));				
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/delete"}, method = RequestMethod.DELETE)
	@ContextJwt
	public ResponseEntity<String> deleteWatch(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value="watchId") String watchId) {
		return treeViewCore.deleteWatchAsApp(hostUser.getHost(), hostUser.getUserAccountId().get(), watchId);
	}

	
	@RequestMapping(value = {"/watch-tree-for"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> treeWatchPageFor(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		JSONObject data = new JSONObject(payload);
		return treeViewCore.setTreeWatchFor(hostUser.getHost(), hostUser.getUserAccountId().get(), data.getString("accountId"), data.getString("pageId"), data.getString("spaceKey"), data.getBoolean("add"), data.getBoolean("setFlags"));
	}
	
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	@IgnoreJwt
	public ResponseEntity<String> status(@RequestParam(value="secret") String secret) {
		if(!secret.equals(statusSecret)) return new ResponseEntity<String>("{}", HttpStatus.FORBIDDEN);
		
		JSONObject status = new JSONObject();
		MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
		status.put("heap", mbean.getHeapMemoryUsage().toString());
		status.put("non-heap", mbean.getNonHeapMemoryUsage().toString());
		status.put("pending", mbean.getObjectPendingFinalizationCount());
		status.put("threads", ManagementFactory.getThreadMXBean().getThreadCount());
		
		return new ResponseEntity<String>(status.toString(), HttpStatus.OK); 
	}
	
	
	@RequestMapping(value = {"/tools/encrypt"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> encrypt(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		JSONObject encrypt = new JSONObject(payload);
		return new ResponseEntity<String>(treeViewSMTP.encryptPassword(encrypt.getString("content")), HttpStatus.OK);
	}
	
	
	@RequestMapping(value = {"/tools/test-smtp"}, method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> testSTMP(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		return new ResponseEntity<String>(treeViewSMTP.sendTestEmail(new JSONObject(payload)).toString(), HttpStatus.OK);
	}
	
	
	@ContextJwt
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ResponseEntity<String> getSettings(@AuthenticationPrincipal AtlassianHostUser hostUser) {
		return new ResponseEntity<String>(safeSettings.getSettings(hostUser.getHost()).toString(), HttpStatus.OK);
	}

	
	@ContextJwt
	@RequestMapping(value = "/settings", method = RequestMethod.PUT)
	public ResponseEntity<String> setSettings(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		JSONObject settings = new JSONObject(payload);
		return safeSettings.setSettings(hostUser, settings);
	}
}
