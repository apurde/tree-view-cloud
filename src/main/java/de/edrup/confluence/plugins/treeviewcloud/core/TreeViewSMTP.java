package de.edrup.confluence.plugins.treeviewcloud.core;

import java.util.Properties;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import de.edrup.confluence.plugins.treeviewcloud.util.MiniJSONDotAccess;
import de.edrup.confluence.plugins.treeviewcloud.util.TreeViewHelper;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

@Service
public class TreeViewSMTP {
    
    @Autowired
    private TreeViewHelper treeViewHelper;
    
    @Value("${STORE_KEY}")
    private String storeKey;

    private MiniJSONDotAccess da = new MiniJSONDotAccess();
    
    private static final Logger log = LoggerFactory.getLogger(TreeViewSMTP.class);
    
    public String encryptPassword(String password) {
        return treeViewHelper.encryptString(password, storeKey);
    }
    
    public JSONObject sendTestEmail(JSONObject details) {
        return sendEmail(
            da.getString(details, "host", ""),
            da.getString(details, "port", ""),
            da.getString(details, "fromAddress", "noreply@example.com"),
            "Testmail",
            da.getString(details, "user", ""),
            da.getString(details, "password", ""),
            da.getString(details, "recipient", ""),
            "Test email from Tree View",
            // Use defaults for text and HTML body if not provided in details.
            da.getString(details, "bodyText", "This is a test email from the Tree View app"),
            da.getString(details, "bodyHTML", "<p>This is a test email from the Tree View app</p>")
        );
    }
    
    public void sendEmail(String to, String subject, String bodyText, String bodyHTML, String fromName, JSONObject settings) {
        sendEmail(
            da.getString(settings, "host", ""),
            da.getString(settings, "port", ""),
            da.getString(settings, "fromAddress", "noreply@example.com"),
            fromName,
            da.getString(settings, "user", ""),
            da.getString(settings, "password", ""),
            to,
            subject,
            bodyText,
            bodyHTML
        );
    }
    
    private JSONObject sendEmail(String host, String port, String fromAddress, String fromName, String user, String password,
                                   String toEmail, String subject, String bodyText, String bodyHTML) {
        try {
            // Create and configure the mail sender dynamically.
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost(host);
            mailSender.setPort(Integer.parseInt(port));
            mailSender.setUsername(user);
            mailSender.setPassword(treeViewHelper.decryptString(password, storeKey));
            
            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.ssl.protocols", "TLSv1.3 TLSv1.2");
            
            // Create a MIME message and configure it using MimeMessageHelper.
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            
            helper.setFrom(new InternetAddress(fromAddress, fromName));
            helper.setTo(InternetAddress.parse(toEmail));
            helper.setSubject(subject);
            // Set both plain text and HTML parts.
            helper.setText(bodyText, bodyHTML);
            
            // Send the email.
            mailSender.send(mimeMessage);
            
            return new JSONObject().put("success", true);
        } catch(Exception e) {
            log.error("Error sending SMTP mail using SMTP server {}: {}", host, e.toString());
            return new JSONObject().put("success", false).put("cause", e.toString());
        }
    }
}
