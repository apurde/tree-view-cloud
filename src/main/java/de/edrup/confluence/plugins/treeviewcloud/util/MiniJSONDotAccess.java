package de.edrup.confluence.plugins.treeviewcloud.util;

import org.json.JSONArray;
import org.json.JSONObject;

public class MiniJSONDotAccess {
	
	public String getString(JSONObject json, String path, String defaultValue) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof String) ? (String) typeObject : defaultValue; 
	}
	
	public Integer getInt(JSONObject json, String path, Integer defaultValue) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof Integer) ? (Integer) typeObject : defaultValue; 
	}
	
	public Long getLong(JSONObject json, String path, Long defaultValue) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof Number) ? ((Number) typeObject).longValue() : defaultValue; 
	}
	
	public Boolean getBoolean(JSONObject json, String path, Boolean defaultValue) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof Boolean) ? (Boolean) typeObject : defaultValue; 
	}
	
	public Float getFloat(JSONObject json, String path, Float defaultValue) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof Float) ? (Float) typeObject : defaultValue; 
	}
	
	public JSONArray getJSONArray(JSONObject json, String path) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof JSONArray) ? (JSONArray) typeObject : new JSONArray(); 
	}
	
	public JSONObject getJSONObject(JSONObject json, String path) {
		Object typeObject = getObject(json, path);
		return (typeObject != null && typeObject instanceof JSONObject) ? (JSONObject) typeObject : new JSONObject(); 
	}

	private Object getObject(JSONObject json, String path) {
		try {
			String[] pathElements = path.split("\\.");
			Object currentObject = json;
			for(int n = 0; n < pathElements.length - 1; n++) {
				String pathElement = pathElements[n];
				currentObject = ((JSONObject) currentObject).get(pathElement);
			}
			return ((JSONObject) currentObject).get(pathElements[pathElements.length - 1]);
		}
		catch(Exception e) {
			return null;
		}
	}
}

