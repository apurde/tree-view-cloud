package de.edrup.confluence.plugins.treeviewcloud.core;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHost;

import de.edrup.confluence.plugins.treeviewcloud.util.MiniJSONDotAccess;
import de.edrup.confluence.plugins.treeviewcloud.util.SafeSettings;
import de.edrup.confluence.plugins.treeviewcloud.util.TreeViewHelper;

@Service
public class TreeViewCore {
	
	@Autowired
	private TreeViewHelper treeViewHelper;
	
	@Autowired
	private TreeViewNotificationService notificationService;
	
	@Autowired
	private SafeSettings safeSettings;
	
	@Autowired
	TaskExecutor executor;
	
	private static final MiniJSONDotAccess da = new MiniJSONDotAccess();
	
	private static final int DESC_LIMIT = 200;
	private static final int WATCH_LIMIT = 50;
	
	private static final Logger log = LoggerFactory.getLogger(TreeViewCore.class);
	
	
	public void setWatchFlags(AtlassianHost host, String accountId, String pageId, Boolean set) {
		String logId = getLogId();
		log.debug("{}: setting watch flags on {} for descendants of page {} and accountId {} to {}", logId, host.getBaseUrl(), pageId, accountId, set);
		CompletableFuture.runAsync(() -> doHandleWatchFlags(host, accountId, pageId, set, logId), executor);
	}
	
	
	public void handlePageAddedOrMovedWebhook(AtlassianHost host, JSONObject webhook) {
		String logId = getLogId();
		try {
			log.debug("{}: webhook received from {}: {}", logId, host.getBaseUrl(), webhook.toString());
			
			JSONObject license = treeViewHelper.getJSONObject(host, null, String.format("%s/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.tree-view",  host.getBaseUrl()));
			if(!da.getBoolean(license, "license.active", true) && !host.getBaseUrl().contains("edrup-dev")) {
				log.info("{}: skipping further actions as app is not licensed", logId);
				return;
			}
			
			String accountId = da.getString(webhook, "accountType", "").equals("app") ? "" : da.getString(webhook, "userAccountId", "");
			String pageId = da.getString(webhook, "page.id", "0");
			String oldParentId = da.getString(webhook, "oldParent.id", "0");
			String spaceKey = da.getString(webhook, "page.spaceKey", "");
			CompletableFuture.runAsync(() -> doHandleBranchAddedOrMoved(host, accountId, pageId, oldParentId.equals("0") ? null : oldParentId, spaceKey, logId), executor);
		}
		catch(Exception e) {
			log.error("{}: could not handle webhook from {} with {}: {}", logId, host.getBaseUrl(), webhook.toString(), e.toString());
		}
	}
	

	public ResponseEntity<String> deleteWatchAsApp(AtlassianHost host, String accountId, String watchId) {
		boolean success = false;
		JSONObject watchObj = treeViewHelper.getJSONObjectAsApp(String.format("%s/api/v2/custom-content/%s", host.getBaseUrl(), watchId));
		if(da.getString(watchObj, "authorId", "unknown").equals(accountId)) {
			success = treeViewHelper.deleteAsApp(String.format("%s/api/v2/custom-content/%s", host.getBaseUrl(), watchId));
		}
		return new ResponseEntity<String>("{}", null, success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
	}


	
	public ResponseEntity<String> setTreeWatchFor(AtlassianHost host, String callingAccounId, String forAccountId, String pageId, String spaceKey, Boolean add, Boolean setWatchFlags) {

		JSONObject pId = treeViewHelper.getJSONObject(host, forAccountId, String.format("%s/api/v2/pages/%s", host.getBaseUrl(), pageId));
		String spaceId = da.getString(pId, "spaceId", "");
		
		if(!treeViewHelper.isAdmin(host, callingAccounId)) {
			if(!isSpaceAdmin(host, callingAccounId, spaceId)) return new ResponseEntity<String>(treeViewHelper.buildJSONErrorMessage("you must be an admin or space admin"), HttpStatus.FORBIDDEN);
		}
		
		JSONObject watches = treeViewHelper.getJSONObject(host, forAccountId, String.format("%s/rest/api/content/search?limit=250&cql=type=\"ac:de.edrup.confluence.plugins.tree-view:treewatch\" and creator=currentUser() and container = %s", host.getBaseUrl(), pageId));

		if(add) {
			if(watches.getJSONArray("results").length() > 0) {
				return new ResponseEntity<String>(treeViewHelper.buildJSONErrorMessage("the tree watch already exists"), HttpStatus.CONFLICT);
			}
			if(setWatchFlags) setWatchFlags(host, forAccountId, pageId, true);

			String buildWStr = buildWatch(pageId).toString();
			
			return treeViewHelper.exchangeStringPayload(host, forAccountId, HttpMethod.POST, String.format("%s/api/v2/custom-content", host.getBaseUrl()), buildWStr);
		}
		else {
			if(watches.getJSONArray("results").length() != 1) return new ResponseEntity<String>(treeViewHelper.buildJSONErrorMessage("the tree watch does not exist"), HttpStatus.NOT_FOUND);
			if(setWatchFlags) setWatchFlags(host, forAccountId, pageId, false);
			return deleteWatchAsApp(host, forAccountId, watches.getJSONArray("results").getJSONObject(0).getString("id"));
		}		
	}
	
	
	private boolean doHandleWatchFlags(AtlassianHost host, String accountId, String pageId, boolean set, String logId) {
		JSONArray descendants = getAllDescendants(host, accountId, pageId, true, logId);
		log.debug("{}: started setting watch flags of {} descendants", logId, descendants.length());
		for(int n = 0; n < descendants.length(); n++) {
			String url = String.format("%s/rest/api/user/watch/content/%s",  host.getBaseUrl(), descendants.getJSONObject(n).getString("id"));
			if(set) {
				treeViewHelper.exchangeStringPayload(host, accountId, HttpMethod.POST, url, "{}");
			}
			else {
				treeViewHelper.delete(host, accountId, url);
			}
		}
		log.debug("{}: completed setting the watch flags", logId);
		return true;
	}
	
		
	private boolean doHandleBranchAddedOrMoved(AtlassianHost host, String accountId, String pageId, String oldParentId, String spaceKey, String logId) {
		JSONArray watches = getAllWatchesAbove(host, accountId, pageId, false, logId);
		JSONArray oldWatches = oldParentId != null ? getAllWatchesAbove(host, accountId, oldParentId, true, logId) : new JSONArray();
		log.debug("{}: detected {} watches and {} watches of the old parent {}", logId, watches.length(), oldWatches.length(), oldParentId);
				
		JSONObject page = null;
		String accountNameTriggering = null;
		if(watches.length() > 0 || oldWatches.length() > 0) {
			page = getPageWithExport(host, accountId, pageId);
			accountNameTriggering = da.getString(getUser(host, accountId), "displayName", "Tree View Cloud");
		}
		
		List<String> notifiedAccountIds = new ArrayList<String>();
		
		JSONObject settings = null;
		if(watches.length() > 0) {
			settings = safeSettings.getSettings(host);
			log.debug("{}: working with the following settings: {}", logId, settings.toString());
		}
		
		for(int n = 0; n < watches.length(); n++) {
			String watchAccountId = da.getString(watches.getJSONObject(n), "history.createdBy.accountId", "unknown");
			String watchAccountName = da.getString(watches.getJSONObject(n), "history.createdBy.displayName", "unknown");
			
			if(hasReadAccess(host, watchAccountId, pageId)) {
				// page added
				if(oldParentId == null) {
					if(!da.getBoolean(settings, "conditionalWatch", false) || isAccountIdWatchingParentPage(host, watchAccountId, pageId, logId)) {
						doHandleWatchFlags(host, watchAccountId, pageId, true, logId);
					}
				}
				// page moved was already part of a watch of the user
				else if(oldParentId != null && accountIdInWatches(oldWatches, watchAccountId)) {
					log.debug("{}: accountId {} already had a watch in a former ancestor - just notifying", logId, watchAccountId);
				}
				// page moved was not part of a watch of the user 
				else {
					log.debug("{}: accountId {} had no watch in a former ancestor", logId, watchAccountId);
					if(!da.getBoolean(settings, "conditionalWatch", false) || isAccountIdWatchingParentPage(host, watchAccountId, pageId, logId)) {
						doHandleWatchFlags(host, watchAccountId, pageId, true, logId);
					}
				}
				if(!watchAccountId.equals(accountId) && !notifiedAccountIds.contains(watchAccountId)) {
					notificationService.notityPageAdded(host, accountNameTriggering, watchAccountId, watchAccountName, page, da.getJSONObject(watches.getJSONObject(n), "container"), settings);
					notifiedAccountIds.add(watchAccountId);
				}
			}
			else {
				log.debug("{}: skipping watch for accountId {} due to missing view permission", logId, watchAccountId);
			}
		}
		
		for(int n = 0; n < oldWatches.length(); n++) {
			String oldWatchAccountId = da.getString(oldWatches.getJSONObject(n), "history.createdBy.accountId", "unknown");
			String oldWatchAccountName = da.getString(oldWatches.getJSONObject(n), "history.createdBy.displayName", "unknown");
			
			if(hasReadAccess(host, oldWatchAccountId, pageId)) {
				// page moved was covered by a user's watch but is not covered by a watch anymore
				if(!accountIdInWatches(watches, oldWatchAccountId)) {
					log.debug("{}: accountId {} had a watch in a former ancestor but no watch in the new ancestors - creating a new watch and notifying", logId, oldWatchAccountId);
					String builtW = buildWatch(pageId).toString();
					treeViewHelper.exchangeStringPayload(host, oldWatchAccountId, HttpMethod.POST, String.format("%s/api/v2/custom-content", host.getBaseUrl()), builtW);
					if(!oldWatchAccountId.equals(accountId) && !notifiedAccountIds.contains(oldWatchAccountId)) {
						notificationService.notityPageMovedOut(host, accountNameTriggering, oldWatchAccountId, oldWatchAccountName, page, da.getJSONObject(oldWatches.getJSONObject(n), "container"), settings);
						notifiedAccountIds.add(oldWatchAccountId);
					}
				}
			}
			else {
				log.debug("{}: skipping watch for accountId {} due to missing view permission", logId, oldWatchAccountId);
			}
		}
		
		return true;
	}
	
	
	private JSONArray getAllDescendants(AtlassianHost host, String useAccountId, String pageId, boolean includeRoot, String logId) {
		JSONArray allDescendants = new JSONArray();
		try {
			int start = 0;
			
			if(includeRoot) {
				JSONObject root = treeViewHelper.getJSONObject(host, useAccountId, String.format("%s/api/v2/pages/%s", host.getBaseUrl(), pageId));
				allDescendants.put(root);
			}
			
			JSONObject batch = treeViewHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/descendant/page?limit=%d", host.getBaseUrl(), pageId, DESC_LIMIT));
			allDescendants.putAll(batch.getJSONArray("results"));
			while(batch.getInt("size") == DESC_LIMIT) {
				start += DESC_LIMIT;
				batch = treeViewHelper.getJSONObject(host, useAccountId, String.format("%s/rest/api/content/%s/descendant/page?start=%d&limit=%d", host.getBaseUrl(), pageId, start, DESC_LIMIT));
				allDescendants.putAll(batch.getJSONArray("results"));
			}
		}
		catch(Exception e) {
			log.error("{}: error getting descendants: {}", logId, e.toString());
			log.trace("Trace: ", e);
		}
		return allDescendants;
	}
	
	
	private JSONArray getAllWatchesAbove(AtlassianHost host, String useAccountId, String pageId, boolean includeRoot, String logId) {
		JSONArray watches = new JSONArray();
		try {
			JSONObject page =  treeViewHelper.getJSONObject(host, useAccountId, String.format("%s/api/v2/pages/%s/ancestors", host.getBaseUrl(), pageId));
			JSONArray ancestors = da.getJSONArray(page, "results");
			
			List<String> ancestorIds = new ArrayList<String>();
			if(includeRoot) {
				ancestorIds.add(pageId);
			}
			for(int n = 0; n < ancestors.length(); n++) { 
				ancestorIds.add(ancestors.getJSONObject(n).getString("id"));
			}
			
			if(ancestorIds.size() > 0) {
				String firstQuery = String.format("%s/rest/api/content/search?limit=%d&expand=history,container&cql=type=\"ac:de.edrup.confluence.plugins.tree-view:treewatch\" and container in (%s)", host.getBaseUrl(), WATCH_LIMIT, String.join(",", ancestorIds));			
				JSONObject batch = treeViewHelper.getJSONObject(host, useAccountId, firstQuery); 
				watches.putAll(batch.getJSONArray("results"));
				while(!da.getString(batch, "_links.next", "").isEmpty()) {
					batch = treeViewHelper.getJSONObject(host, useAccountId, host.getBaseUrl() + URLDecoder.decode(da.getString(batch, "_links.next", ""), "UTF-8")); 
					watches.putAll(batch.getJSONArray("results"));
				}
			}
		}
		catch(Exception e) {
			log.error("{}: error getting watches: {}", logId, e.toString());
			log.trace("Trace: ", e);
		}
		return watches;
	}
	
	
	private String getLogId() {
		return UUID.randomUUID().toString();
	}
	
	
	private boolean accountIdInWatches(JSONArray watches, String accountId) {
		for(int n = 0; n < watches.length(); n++) {
			if(da.getString(watches.getJSONObject(n), "history.createdBy.accountId", "unknown").equals(accountId)) return true;
		}
		return false;
	}
	
	
	private boolean isAccountIdWatchingParentPage(AtlassianHost host, String accountId, String pageId, String logId) {
		try {
			JSONObject page =  treeViewHelper.getJSONObject(host, accountId, String.format("%s/api/v2/pages/%s/ancestors", host.getBaseUrl(), pageId));
			JSONArray ancestors = da.getJSONArray(page, "results");
			if(ancestors.length() > 0) {
				String parentPageId = ancestors.getJSONObject(ancestors.length() - 1).getString("id");
				return da.getBoolean(treeViewHelper.getJSONObject(host, accountId, String.format("%s/rest/api/user/watch/content/%s", host.getBaseUrl(), parentPageId)), "watching", false);
			}
		}
		catch(Exception e) {
			log.error("{}: error getting watch status of accountId {} for the parent of {}: {}", logId, accountId, pageId, e.toString());
			log.trace("Trace: ", e);
		}
		return false;
	}
	
	
	private JSONObject buildWatch(String pageId) {
		JSONObject watch = new JSONObject();
		watch.put("title", "Tree Watch");
		watch.put("status", "current");
		watch.put("body", new JSONObject().put("value", "{\"active\":true}").put("representation", "storage"));
		watch.put("type", "ac:de.edrup.confluence.plugins.tree-view:treewatch");
		watch.put("pageId", pageId);
		return watch;
	}
		
	
	private JSONObject getPageWithExport(AtlassianHost host, String accountId, String pageId) {
		return treeViewHelper.getJSONObject(host, accountId, String.format("%s/api/v2/pages/%s?body-format=export_view", host.getBaseUrl(), pageId));
	}
	
	
	private JSONObject getUser(AtlassianHost host, String accountId) {
		return treeViewHelper.getJSONObject(host, accountId, String.format("%s/rest/api/user/current", host.getBaseUrl()));
	}
	
	
	private boolean hasReadAccess(AtlassianHost host, String accountId, String contentId) {
		return treeViewHelper.getJSONObject(host, accountId, String.format("%s/api/v2/pages/%s", host.getBaseUrl(), contentId)).has("id");
	}
	
	
	private boolean isSpaceAdmin(AtlassianHost host, String accountId, String spaceId) {
		try {
			JSONArray spacePermissions =  treeViewHelper.getJSONObject(host, accountId, String.format("%s/api/v2/spaces/%s/permissions", host.getBaseUrl(), spaceId)).getJSONArray("results");
			JSONArray membershipsOfAccID = treeViewHelper.getJSONObject(host, accountId, String.format("%s/rest/api/user/memberof?accountId=%s", host.getBaseUrl(), accountId)).getJSONArray("results");

			for (int n = 0; n < spacePermissions.length(); n++) {
				if (da.getString(spacePermissions.getJSONObject(n), "operation.key", "").equals("administer")) {
					
					String groupId = da.getString(spacePermissions.getJSONObject(n), "principal.id", "");
	
					for (int m = 0; m < membershipsOfAccID.length(); m++) {
						if (da.getString(membershipsOfAccID.getJSONObject(m), "id", "not-found-2").equals(groupId)) {
							 return true;
						}
					}
				}
			}
			return false;
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		return false;
	}
}
