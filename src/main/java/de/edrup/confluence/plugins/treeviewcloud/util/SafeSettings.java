package de.edrup.confluence.plugins.treeviewcloud.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class SafeSettings {
	
	@Autowired
	private TreeViewHelper treeViewHelper;
	
	@Value("${SALT}")
    private String salt;
	
	private static final Logger log = LoggerFactory.getLogger(SafeSettings.class);
	
	
	public JSONObject getSettings(AtlassianHost host) {
		JSONObject settings = null;
		try {
			settings = treeViewHelper.getJSONObjectAsApp(host.getBaseUrl() + "/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.tree-view/properties/tree-view-settings?jsonValue=true").getJSONObject("value");
		}
		catch(Exception e) {
			settings = new JSONObject();
		}
		settings.put("hashValid", areSettingsValid(host, settings));
		return settings;
	}
	
	
	public ResponseEntity<String> setSettings(AtlassianHostUser hostUser, JSONObject settings) {
		if(treeViewHelper.isAdmin(hostUser.getHost(), hostUser.getUserAccountId().orElse("unknown"))) {
			settings.put("hash", calculateIndividualSettingsHash(hostUser.getHost(), settings));
			return treeViewHelper.exchangeStringPayload(hostUser.getHost(), hostUser.getUserAccountId().orElse(null), HttpMethod.PUT, hostUser.getHost().getBaseUrl() +  "/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.tree-view/properties/tree-view-settings", settings.toString());
		}
		else {
			return new ResponseEntity<String>("", HttpStatus.FORBIDDEN);
		}
	}
	
	
	private boolean areSettingsValid(AtlassianHost host, JSONObject settings) {
		String hash = calculateIndividualSettingsHash(host, settings);
		log.debug("Current settings for host {} are valid {} with migratedNotSaved {}", host.getBaseUrl(), settings.optString("hash").equals(hash), settings.optBoolean("migratedNotSaved"));
		return hash.equals(settings.optString("hash"));
	}
	
	
	private String calculateIndividualSettingsHash(AtlassianHost host, JSONObject settings) {
		String stringToHash = settings.optString("mailServer") + settings.optString("host") + settings.optString("port") + settings.optString("user") + settings.optString("password") +
			host.getClientKey() + salt;
		return SHA256Calc(stringToHash);		
	}
	
	
	private String SHA256Calc(String value) {
		byte[] bytesOfValue;
		try {
			bytesOfValue = value.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			return e.toString();
		}
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] sha = md.digest(bytesOfValue);
			return bytesToHex(sha);
		} catch (NoSuchAlgorithmException e) {
			return e.toString();
		}
	}
	
	
	private String bytesToHex(byte[] in) {
	    final StringBuilder builder = new StringBuilder();
	    for(byte b : in) {
	        builder.append(String.format("%02x", b));
	    }
	    return builder.toString();
	}
}
