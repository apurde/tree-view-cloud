function TreeWatchDialog() {
	
	var sharedFunctions = null;
	var spaceKey = $("#tree-watch-dialog").attr("data-space-key");
	var pageId = $("#tree-watch-dialog").attr("data-page-id");
	var accountId =  $("#tree-watch-dialog").attr("data-account-id");
	var activeWatchId = null;
	var changedToggle = false;


	this.init = function(shared) {
		sharedFunctions = shared;
		
		$("#tree-watch-apply").click(function() {
			applyWatch(function() {
				AP.dialog.close({});
			});
		});
		
		$("#tree-watch-cancel").click(function() {
			AP.dialog.close({});
		});
		
		initUI();
	};
	
	
	var initUI = function() {
		$("#tree-watch-toggle")[0].busy = true;
		sharedFunctions.getActiveWatchId(pageId, accountId, function(watchId) {
			$("#tree-watch-toggle")[0].busy = false; 
			if(watchId) {
				activeWatchId = watchId;
				$("#tree-watch-toggle").prop("checked", true);	
			}
			$("#tree-watch-toggle").change(function() {
				changedToggle = !changedToggle;
				$("#tree-watch-apply").attr("disabled", !changedToggle);
				$("#tree-watch-apply").attr("aria-disabled", !changedToggle);
			});
		});
	};
	
	
	var applyWatch = function(callback) {
		if($("#tree-watch-toggle").prop("checked")) {
			sharedFunctions.setWatch(spaceKey, pageId, accountId, function(success) {
				if(success) {
					sharedFunctions.startWatchFlagSetting(pageId, true);
					callback();		
				}
			});		
		}
		else {
			sharedFunctions.removeWatch(activeWatchId, function(success) {
				if(success) {
					sharedFunctions.startWatchFlagSetting(pageId, false);
					callback();		
				}
			});
		}
	};
}

var treeWatchDialog = new TreeWatchDialog();
var sharedFunctions = new SharedFunctions();
treeWatchDialog.init(sharedFunctions);
