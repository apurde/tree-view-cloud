/*jshint esversion: 9 */

function TreeViewConfigure() {
	
	var passwordDirty = false;
	var passwordEncrypted = "";

	
	this.init = function() {
		$('#saveInProgress').hide();
		
		getSettingsViaApp(initGUI);
		
		$("#password").on("keydown", function() {
			passwordDirty = true;	
		});
		
		$("#mail-server").change(function() {
			$("#external").toggle($("#mail-server").val() === "external");
		});
		
		$("#save").click(function() {
			saveSettings();
		});
		
		$("#delete-settings").click(function() {
			AP.request({
				url: '/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.tree-view/properties/tree-view-settings',
				type: 'DELETE'
			});
		});
		
		$("#test").click(function() {
			sendTestMail();
		});
	};
	
	
	var saveSettingsViaApp = function(settings) {		
		AP.context.getToken(function(token){
			$.ajax({
				url: "/settings",
				type: "PUT",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "JWT " + token);
				},
				data: JSON.stringify(settings),
				contentType: "application/json",
			    success: function() {
                	$('#saveInProgress').hide();
                }
			});
		});
	};
	 
	 
	var getSettingsViaApp = function(callback) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/settings",
				type: "GET",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(responseText) {
					var response = JSON.parse(responseText);
					callback(response);
				},
				error: function() {
					callback({});
				}
			});
		});
	};
	 
	 
	var initGUI = function(settings) {
		$("#added-title").val(settings.addedTitle || $("#defaultAddedTitle").text());
		$("#added-body").val(settings.addedBody ||  $("#defaultAddedBody").text());
		$("#movedout-title").val(settings.movedoutTitle || $("#defaultMovedoutTitle").text());
		$("#movedout-body").val(settings.movedoutBody || $("#defaultMovedoutBody").text());
		$("#conditional-watch").prop("checked", settings.conditionalWatch || false);
		$("#mail-server").val(settings.mailServer || "internal").trigger("change");
		$("#from-address").val(settings.fromAddress || "");
		$("#host").val(settings.host || "");
		$("#port").val(settings.port|| "587");
		$("#user").val(settings.user|| "");
		if(settings.password && settings.password.length > 0) {
			$("#password").attr("placeholder", $("#password").data("set-password"));
			$("#password").val("");
			passwordEncrypted = settings.password;
		}
 	};
 	
 	
 	var saveSettings = async function() {
		$('#saveInProgress').show();
	 	var settings = await getSettings();
		saveSettingsViaApp(settings);
	};
	
	
	var getSettings = async function() {
		
		if(passwordDirty &&  $("#mail-server").val() === "external") {
			passwordEncrypted = await encryptPasswort($("#password").val());
			passwordDirty = false;
			$("#password").attr("placeholder", $("#password").data("set-password"));
			$("#password").val("");				
		}
	 	
	 	return {
	 		addedTitle: $("#added-title").val(),
	 		addedBody: $("#added-body").val(),
	 		movedoutTitle: $("#movedout-title").val(),
	 		movedoutBody: $("#movedout-body").val(),
	 		conditionalWatch: $('#conditional-watch').prop('checked'),
			mailServer : $("#mail-server").val(),
			fromAddress : $("#from-address").val(),
			host : $("#host").val(),
			port : $("#port").val(),
			user : $("#user").val(),
			password : passwordEncrypted
	 	};
	};
	
	
	var encryptPasswort = function(password) {
		return new Promise((resolve, reject) => {
			var payload = {
				content : password
			};
			AP.context.getToken(function(token){
				$.ajax({
					url: "/tools/encrypt",
					type: "POST",
					beforeSend: function (request) {
						request.setRequestHeader("Authorization", "JWT " + token);
					},
					data: JSON.stringify(payload),
					contentType: "application/json",
					success: function(result) {
						return resolve(result);
					}
				});
			});
		});
	};
	
	
	var sendTestMail = async function() {
		var settings = await getSettings();
		settings.recipient = $("#recipient").val();
		AP.context.getToken(function(token){
			$.ajax({
				url: "/tools/test-smtp",
				type: "POST",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "JWT " + token);
				},
				data: JSON.stringify(settings),
				contentType: "application/json",
				success: function(resultText) {
					var result = JSON.parse(resultText);
					if(result.success) {
						AP.flag.create({
							title: $("#testSuccessTitle").text(),
							body: $("#testSuccessBody").text(),
							type: "success",
							close: "auto"
						});
					}
					else {
						AP.flag.create({
							title: $("#testErrorTitle").text(),
							body: result.cause,
							type: "error",
							close: "auto"
						});
					}
				}
			});
		});
	};
}


var treeViewConfigure = new TreeViewConfigure();
treeViewConfigure.init();