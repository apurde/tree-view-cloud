/*jshint esversion: 9 */

function TreeViewMigration() {
	
	var accountIds = {};

	this.init = function() {
		
		$("#simulate").click(function() {
			$("#results").empty();
			doMigrate(true);
		});
		
	
		$("#migrate").click(function() {
			$("#results").empty();
			doMigrate( false);
		});		
	};
	
	
	var doMigrate = async function(simulate) {
		try {
			$("#migration-spinner").show();
			$("#simulate,#migrate").hide();
	
			logInfo("Starting migration");
			
			accountIds = {};
			var source = JSON.parse($("#source").val());
			
			var promises = [];
			source.treeWatch.forEach(watch => promises.push(migrateWatch(watch, simulate)));
			await Promise.all(promises);
		}
		catch(error) {
			logError(error.message);	
		}
		
		logInfo("Migration completed");
		$("#migration-spinner").hide();
		$("#simulate,#migrate").show();
	};
	
	
	var migrateWatch = async function(watch, simulate) {
		try {
			var accountId = await findAccountId(watch.fullname);
			var pageId = await findPageId(watch.title, watch.spacekey);
			if(accountId && pageId) {
				if(!simulate) {
					var watchResult = await setWatch(accountId, pageId, watch.spacekey);
					if(!watchResult.success) {
						if(watchResult.status === 409) {
							logInfo("Tree watch of user " + watch.fullname + " on page " + watch.title + " in space " + watch.spacekey  + " already exists");	
						}
						else {
							logError("Could not set tree watch for user " + watch.fullname + " on page " + watch.title + " in space " + watch.spacekey  + ": " + watchResult.error);
						}
					}
					else {
						logInfo("Migrated tree watch of user " + watch.fullname + " on page " + watch.title + " in space " + watch.spacekey);
					}
				}
				else {
					logInfo("Simluated migration of tree watch of user " + watch.fullname + " on page " + watch.title + " in space " + watch.spacekey);	
				}
			}
		}
		catch(error) {
			logError("Error migrating watch " + JSON.stringify(watch) + ": " + error.message);
		}
	};
	
	
	var findAccountId = async function(fullName) {
		if(accountIds[fullName]) return accountIds[fullName];
		var searchRequest = await AP.request("/rest/api/search/user?cql=user.fullname~'" + fullName + "'");
		var searchResults = JSON.parse(searchRequest.body);
		if(searchResults.results.length === 1) {
			var accountId = searchResults.results[0].user.accountId;
			accountIds[fullName] = accountId;
			return accountId;
		}
		else {
			logError("Error finding accountId for user " + fullName + ". Length of results was " + searchResults.results.length + ".");	
			return null;
		}
	};
	
	var getSpaceIdFromKey = async function(params) {
		try {
			var spaceRequest = await AP.request("/api/v2/spaces?keys=" + params.spaceKey);
			var space = JSON.parse(spaceRequest.body);
			if(params.success) params.success(space.results[0].id);
			return space.results[0].id;
		}
		catch(error) {
			if(params.error) params.error();
			throw error;
		}
	};

	var findPageId = async function(pageTitle, spaceKey) {
		try {
			var spaceId = await getSpaceIdFromKey({"spaceKey": spaceKey});
			var pageRequest = await AP.request("/api/v2/spaces/"+spaceId+"/pages?title=" + encodeURIComponent(pageTitle));
			var pages = JSON.parse(pageRequest.body);

			if(pages.results.length == 1) {
				return pages.results[0].id;
			} 

			logError("Error finding page with title " + pageTitle + " in space with key " +  spaceKey);	
			return null;
		}
		catch(error) {
			logError("Error finding page with title " + pageTitle + " in space with key " +  spaceKey);	
			return null;
		}
	};
	
	
	var setWatch = async function(accountId, pageId, spaceKey) {
		return new Promise((resolve, reject) => {
			var bodyData = {
				accountId : accountId,
				pageId : pageId,
				spaceKey : spaceKey,
				add : true,
				setFlags: false
			};
			AP.context.getToken(function(token){
				$.ajax({
					url: "/watch-tree-for",
					type: "POST",
					data: JSON.stringify(bodyData),
					contentType : "application/json",
					beforeSend: function (request) {
						request.setRequestHeader("Authorization", "JWT " + token);
					},
					success: function() {
						return resolve({success : true});
					},
					error: function(request, status, error) {
						return resolve({
							success : false,
							status : request.status,
							error: buildErrorMessage(request, status, error)
						});
					}
				});
			});	
		});		
	};
		
	
	var sanitizeText = function(text) {
		return $('<div/>').text(text).html();
	};
	
	
	var logError = function(message) {
		console.error(message);
		addToResults(message, "migration-error");
	};
	
	
	var logInfo = function(message) {
		addToResults(message, "");
	};
	
	
	var addToResults = function(message, color) {
		// security hint: color is only defined within this JS file, the message could contain malicious code and is therefore escaped
		$("#results").append('<p class="' + color + '">' + sanitizeText(message) + '</p>');
	};
	
	
	var buildErrorMessage = function(request, status, error) {
		var message = request.status + ";" + status + ";" + error;
		try {
			var response = JSON.parse(request.responseText);
			message = response.error || message;	
		}
		catch(error) {}
		return message;
	};

}

var treeViewMigration = new TreeViewMigration();
treeViewMigration.init();
