function TreeViewResolver() {
	
	this.resolve = function() {
		AP.request({
			url: "/api/v2/custom-content/" + $("#tree-view-resolver-container").attr("data-content-id"),
			success: function(resultText) {
				var result = JSON.parse(resultText);
				AP.navigator.go('contentview', {contentId: result.pageId});
			},
			error: function() {
				$("#simple-qa-resolver-container").hide();
				AP.flag.create({
					title: $("#errorTitle").text(),
					body: $("#errorResolve").text(),
					type: "error",
					close: "auto"
				});
			}
		});
	};
}

var treeViewResolver = new TreeViewResolver();
treeViewResolver.resolve();
