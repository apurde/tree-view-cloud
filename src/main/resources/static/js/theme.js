const themeObserver = new MutationObserver(function() {
	if($(document.documentElement).attr("data-color-mode")) {
		$("body").addClass("aui-theme-design-tokens");
	}
	else {
		$("body").removeClass("aui-theme-design-tokens");
	}
});
themeObserver.observe(document.documentElement, { attributes: true, childList: false, subtree: false });
window.AP.theming.initializeTheming();