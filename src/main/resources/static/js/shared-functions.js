function SharedFunctions() {
	
	
	this.sanitizeText = function(text) {
		return $('<div/>').text(text).html();
	};
	
	
	this.getActiveWatchId = function(pageId, accountId, callback) {
		doGetActiveWatchId(pageId, accountId, callback);
	};
	
	
	this.setWatch = function(spaceKey, pageId, accountId, callback) {
		doGetActiveWatchId(pageId, accountId, function(watchId) {
			if(watchId === null) {
				doSetWatch(spaceKey, pageId, callback);	
			}
			else {
				showErrorFlag($("#errorSetWatchConflict").text());
			}
		});
	};
	
	
	this.removeWatch = function(watchId, callback) {
		AP.request({
			url: "/api/v2/custom-content/" + watchId,
			type: "DELETE",
			success: function() {
				showSuccessFlag($("#successDeleteWatch").text());
				callback(true);
			},
			error: function() {
				removeWatchByApp(watchId, function(result) {
					callback(result);
				});
			}
		});		
	};
	
	
	this.startWatchFlagSetting = function(pageId, set) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/watch-tree?set=" + set + "&pageId=" + pageId,
				type: "POST",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "JWT " + token);
				}
			});
		});			
	};
	
	
	var doGetActiveWatchId = async function(pageId, accountId, callback) {
		var allWatches = await getAllWatches(pageId);
		var activeWatches = allWatches.filter(watch => watch.authorId === accountId);
		if(activeWatches.length > 0) {
			if(activeWatches.length > 1) {
				console.error("User %s has %i active tree watches on page %s", accountId, activeWatches.length, pageId);
			}
			callback(activeWatches[0].id);
		}
		else {
			callback(null);
		}
	};
	
	
	var getAllWatches = async function(pageId) {
		var watches = [];
		
		var url = "/api/v2/pages/" + pageId + "/custom-content?type=ac:de.edrup.confluence.plugins.tree-view:treewatch&limit=250";
		var hasNext = true;
		
		while(hasNext) {
			var searchRequest = await AP.request(url);
			var searchResults = JSON.parse(searchRequest.body);
			watches = watches.concat(searchResults.results);
			url = searchResults._links.next || "";
			hasNext = url.length > 0;
		}
		
		return watches;
	};
	
	
	var doSetWatch = function(spaceKey, pageId, callback) {
		var watch = {
			active: true	
		};
	
		var bodyData = {
			type: "ac:de.edrup.confluence.plugins.tree-view:treewatch",
			status: "current",
			pageId: pageId,
			title : "TreeWatch",
			body: {
				storage: {
					value: JSON.stringify(watch),
					representation: "storage"
				}
			}			
			
		};

		AP.request({
			url: "/api/v2/custom-content",
			type: "POST",
			data: JSON.stringify(bodyData),
			contentType : "application/json",
			dataType : "json",
			success: function(result) {
				showSuccessFlag($("#successSetWatch").text());
				callback(true);
			},
			error: function() {
				showErrorFlag($("#errorSetWatch").text());
				callback(false);
			}
		});		
	};
	
	
	var removeWatchByApp = function(watchId, callback) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/delete?watchId=" + watchId,
				type: "DELETE",
				beforeSend: function (request) {
					request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function() {
					showSuccessFlag($("#successDeleteWatch").text());
					callback(true);
				},
				error: function() {
					showErrorFlag($("#errorDeleteWatch").text());
					callback(false);
				}
			});		
		});
	};
	
	
	var showErrorFlag = function(bodyText) {
		AP.flag.create({
			title: $("#errorTitle").text(),
			body: bodyText,
			type: "error",
			close: "auto"
		});
	};
	
	
	var showSuccessFlag = function(bodyText) {
		AP.flag.create({
			body: bodyText,
			title: $("#successTitle").text(),
			type: "success",
			close: "auto"
		});
	};
}