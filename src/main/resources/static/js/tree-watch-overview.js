function TreeWatchOverview() {
	
	var sharedFunctions = null;
	var baseUrl = $("#tree-watches-all").attr("data-base-url");
	
	
	this.init = function(shared) {
		sharedFunctions = shared;
		getWatchesOfUser();
	};
	
	
	var getWatchesOfUser = function() {
		AP.request({
			url: '/rest/api/content/search?limit=250&expand=container,space&cql=type="ac:de.edrup.confluence.plugins.tree-view:treewatch" and creator=currentUser() order by title',
			success: function(resultText) {
				displayPagesWithWatch(orderByPageTitle(JSON.parse(resultText).results));
			}
		});
	};
	
	
	var orderByPageTitle = function(watches) {
		return watches.sort(function(a, b) {
			return a.container.title.localeCompare(b.container.title);
		});
	};
	
	
	var displayPagesWithWatch = function(watches) {
		$("#tree-watch-overview-spinner").hide();
		watches.forEach(function(watch) {
			var pageNode = '<p>' + 
					'<a target="_top" href="' + baseUrl + watch.container._links.webui + '">' + sharedFunctions.sanitizeText(watch.container.title) + '</a>' +
					' (' + sharedFunctions.sanitizeText(watch.space.name) + ')' + 
					' <button type="button" class="aui-button aui-button-link stop-watching" data-watch-id="' + watch.id + '" data-page-id="' + watch.container.id + '">' + $("#stopWatching").text() + "</button>" +
				'</p>';
			$("#tree-watches-all").append(pageNode);
		});
		if(watches.length === 0) {
			$("#no-tree-watches").show();
		}
		$(".stop-watching").click(function() {
			var element = this;
			sharedFunctions.removeWatch($(element).attr("data-watch-id"), function(success) {
				if(success) {
					sharedFunctions.startWatchFlagSetting($(element).attr("data-page-id"), false);
					$(element).closest("p").remove();
				}
			});
		});
	};
}

var treeWatchOverview = new TreeWatchOverview();
var sharedFunctions = new SharedFunctions();
treeWatchOverview.init(sharedFunctions);