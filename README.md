# Tree View for Confluence Cloud app
Watch and un-watch a complete tree of pages (a page and all its descendants) with just one action

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer [this page](https://purde-software.atlassian.net/wiki/x/CAD4q).

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the "master" branch.

## Change log

### 1.6.2

* Upgrade to Spring Boot 3.4.3
* Upgrade to ACSB 5.1.7
* Upgrade other dependencies
* Switch to Spring Mail
* Suggest TLS v1.3 as default of mail servers
* Improve encryption code
* Always handle spaceId as String

### 1.6.1

* Upgrade to Spring Boot 3.4.1
* Upgrade to ACSB 5.1.5

### 1.6.0

* Switch to REST API v2

### 1.5.9

* Upgrade dependencies

### 1.5.8

* Fix delete media type issue
* Upgrade dependencies

### 1.5.7

* Upgrade dependencies

### 1.5.6

* Upgrade dependencies

### 1.5.5

* Upgrade to Spring Boot 3.1.5
* Upgrade to ACSB 4.0.5

### 1.5.4

* Upgrade to Spring Boot 2.7.16
* Upgrade to ACSB 3.0.7
* Change icon color for better visibility in Confluence's dark thema

### 1.5.3

* Switch to safe settings to improve security

### 1.5.2

* Properly detect no-theme

### 1.5.1

* Upgrade to Spring Boot 2.7.14
* Override Thymeleaf version to fix a vulnerability

### 1.5.0

* Upgrade to Spring Boot 2.7.13
* Upgrade to ACSB 3.0.4
* Support Confluence's Dark mode

### 1.4.3

* Upgrade to Spring Boot 2.7.11
* Upgrade to ACSB 3.0.3
* Upgrade to json 20230227

### 1.4.2

* Upgrade to Spring Boot 2.7.10
* Remove googleapis
* Base test system on Java 17
* Use TaskExecutor for async calls

### 1.4.1

* Upgrade to Spring Boot 2.7.8
* Upgrade to ACSB 3.0.2
* Mask email addresses in logs

### 1.4.0

* Check license status after webhook received
* Allow configuration of external mail servers
* Upgrade to Spring Boot 2.7.7

### 1.3.6

* Upgrade aws-java-sdk-ses

### 1.3.5

* Upgrade postgresql
* Upgrade to Spring Boot 2.7.6

### 1.3.4

* Upgrade postgresql

### 1.3.3

* Use runAsync instead of supplyAsync

### 1.3.2

* Change autowired instances to private

### 1.3.1

* Upgrade to Spring Boot 2.7.2
* Add status endpoint

### 1.3.0

* Upgrade to Spring Boot 2.7.1
* Upgrade to ACSB 2.3.6
* Remove 1000 watches limit
* Add migration UI
* Add CSP header
* Remove inline JS parts

### 1.2.4

* Upgrade to Spring Boot 2.6.8

### 1.2.3

* Switch to cloud based AUI

### 1.2.2

* Upgrade to Atlassian Connect Spring Boot 2.3.4

### 1.2.1

* Check view permission earlier

### 1.2.0

* Execute actions following webhooks coming from apps as our app
* Update other dependencies

### 1.1.3 (not tagged)

* Upgrade to Atlassian Connect Spring Boot 2.3.3
* Upgrade to Spring Boot 2.6.6

### 1.1.2

* Upgrade to Atlassian Connect Spring Boot 2.3.1
* Upgrade to Spring Boot 2.6.4
* Upgrade other dependencies

### 1.1.1

* Upgrade to Spring Boot 2.5.10

### 1.1.0

* Upgrade to Atlassian Connect Spring Boot 2.2.6
* Upgrade to Spring Boot 2.5.9
* Add ACCESS_EMAIL_ADDRESSES as scope to allow mail notifications

### 1.0.0

* Initial release